# import encode from jwt
from jwt import decode, encode


# create a function to generate a token with a dictionary payload as input
def generate_token(payload: dict) -> str:
    # create a variable called token that use encode function to generate a token with HS256 algorithm and must use the payload as input and have a secret key "my_secret_key"
    token = encode(payload, key="my_secret_key", algorithm="HS256")
    return token


# create a function to validate a token with a token as input
def validate_token(token: str) -> dict:
    # create a variable called data that is a dictionary. This value of the varible is generate by the function decode tnat use the token , the ky "my_secret_key" and the algorithm HS256
    data = decode(token, key="my_secret_key", algorithms=["HS256"])
    return data
