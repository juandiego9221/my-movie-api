# add missing imports
from typing import Optional
from pydantic import BaseModel, Field


# create a class Movie from base model
class Movie(BaseModel):
    id: Optional[int] = None
    # id: int | None = None
    title: str = Field(min_length=5, max_length=50)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(ge=1900, le=2022)
    rating: float
    category: str

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Pelicula default",
                "overview": "Descripcion de la pelicula",
                "year": 2020,
                "rating": 4.5,
                "category": "action",
            }
        }
