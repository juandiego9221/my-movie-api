# add missing imports
from pydantic import BaseModel, Field


# create a class User from base model
class User(BaseModel):
    email: str = Field(min_length=1, max_length=50)
    password: str = Field(min_length=1, max_length=50)
