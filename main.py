# create and instance of the FastAPI class calle app
from fastapi import FastAPI

# import optional
from config.database import engine, Base

# import errorhandler from middlewares
from middlewares.error_handler import ErrorHandler

from routers.movie import movie_router

# import the user_router
from routers.user import user_router


"""
1. The "fastapi" module is a popular Python web framework for building APIs quickly and easily.
2. Within the "fastapi" module, there is a sub-module called "encoders" which provides functions for encoding and decoding data in various formats.
3. The "jsonable_encoder" function is one such function that takes a Python object and returns its JSON serializable equivalent.
4. This function is very useful when working with APIs because many APIs require data to be in JSON format. By using "jsonable_encoder", we can easily encode our Python objects into JSON format without having to write custom serialization code.
"""

"""
Por supuesto. La línea de código que has proporcionado en Python se utiliza para crear automáticamente las tablas en la base de datos según los modelos definidos utilizando SQLAlchemy. A continuación, desglosaré cada parte de la expresión:

Al llamar a Base.metadata.create_all(bind=engine), SQLAlchemy examina los modelos definidos en Base y crea las tablas correspondientes en la base de datos si aún no existen. Este proceso se conoce como "creación de esquema" o "migración inicial".

Base.metadata: En SQLAlchemy, Base es una clase base comúnmente utilizada para definir los modelos de datos. metadata es un atributo de la clase Base que almacena metadatos sobre los modelos, como la estructura de las tablas y las relaciones entre ellas.

create_all(bind=engine): create_all() es un método proporcionado por el atributo metadata que genera y crea todas las tablas definidas en los modelos asociados a Base. El argumento bind se establece en el objeto Engine (engine) que representa la conexión a la base de datos. Esto vincula la creación de tablas a esa conexión específica.
"""
Base.metadata.create_all(bind=engine)

app = FastAPI()
# add a title to the instance app called "My aplicacion con fastapi"
app.title = "My aplicacion con fastapi"
# add a version to the instance app called "0.0.1"
app.version = "0.0.1"

"""
This code adds an error handling middleware to the app.
 Step by step explanation:
 1. The code starts by calling the "middleware" function on the "app" object.
2. The "ErrorHandler" argument passed to the "middleware" function is a reference to a middleware function that is specifically designed to handle errors.
3. The "middleware" function adds this error handling middleware to the app's middleware chain, so that any errors that occur during the app's execution will be caught and handled by the "ErrorHandler" middleware.
4. The exact behavior of the "ErrorHandler" middleware will depend on how it is implemented, but generally it will include logic to log the error, provide a helpful error message to the user, and/or take other corrective actions as needed.
"""
app.middleware(ErrorHandler)

app.include_router(movie_router)
# add the orger user_router to the app
app.include_router(user_router)


# generate a get operation called message that return a string message "hello world"
@app.get("/", tags=["home"])
def message():
    # esto es un diccionario
    return {"message": "Hello World"}
