# import basehttpmiddleware from starlette
from starlette.middleware.base import BaseHTTPMiddleware

# import FastAPI from fastapi
from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse


class ErrorHandler(BaseHTTPMiddleware):
    # create constructor and use FastAPI as app
    def __init__(self, app: FastAPI):
        # call the constructor of the super class
        super().__init__(app)
        # assign the app to the variable app
        # self.app = app

    """
The code defines an asynchronous function called dispatch that takes in a request object and a call_next function as arguments. The function tries to call the call_next function with the request object and returns the result. If an exception occurs during the call, a JSONResponse object is returned with a status code of 500 and an error message.
Step-by-step explanation:
1. The code starts by defining an asynchronous function called dispatch. The function takes in two arguments - a request object and a call_next function.
2. The function tries to call the call_next function with the request object using the await keyword to pause execution until the function returns.
3. If the call to the call_next function raises an exception, it is caught by an except block.
4. The except block then creates a JSONResponse object with a status code of 500 and an error message. The error message includes the details of the exception that was raised, converted to a string using the str() function.
5. The JSONResponse object is then returned. 
In summary, the dispatch function is a wrapper function that tries to call a provided function with a request object and returns its result. If an exception is raised during the call, it returns a JSONResponse object with an error message.
    """
    # create a function dispatch that receive a request and a call_next as input
    async def dispatch(self, request: Request, call_next) -> Response | JSONResponse:
        try:
            return await call_next(request)
        except Exception as e:
            return JSONResponse(
                status_code=500,
                content={"message": "Internal Server Error", "detail": str(e)},
            )
