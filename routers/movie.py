from http.client import HTTPResponse
from fastapi import APIRouter
from fastapi import Depends, HTTPException, Path, Query
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from model.movie import Movie as MovieModel
from middlewares.jwt_bearer import JWTBearer

# create and instance of the FastAPI class calle app
from fastapi import Depends, HTTPException, Path, Query

# import htmlresponse
from fastapi.responses import JSONResponse

# import pydantic

# import optional
from typing import List
from config.database import Session
from model.movie import Movie as MovieModel

# import errorhandler from middlewares
from middlewares.jwt_bearer import JWTBearer
from fastapi.encoders import jsonable_encoder

# import movieservice class from service package
from services.movie import MovieService

# import movie schema from schema package
from schemas.movie import Movie

"""
The above code creates a new instance of an APIRouter called "movie_router".
 Explanation:
- "APIRouter" is a class provided by a Python package called FastAPI.
- This package is used for building web APIs with Python.
- An APIRouter is an object that helps create and organize endpoints and API operations. - IMPORTANTE
- The code creates a new instance of an APIRouter called "movie_router". This can be used to define endpoint and API operations related to movies.
"""
movie_router = APIRouter()
# create a new variable called movies with a list of dictionaries. each dictionary must have these keys: id,title,overview,year,rating,category
movies = [
    {
        "id": 1,
        "title": "Spiderman",
        "overview": "Spiderman is a superhero",
        "year": 2019,
        "rating": 4.5,
        "category": "action",
    },
    # generate 2 more dictionaries with the same keys
    {
        "id": 2,
        "title": "Avengers",
        "overview": "Avengers is a superhero",
        "year": 2019,
        "rating": 4.5,
        "category": "action",
    },
    {
        "id": 3,
        "title": "Batman",
        "overview": "Batman is a superhero",
        "year": 2019,
        "rating": 4.5,
        "category": "action",
    },
    # add another movie but with a comedy category
    {
        "id": 4,
        "title": "The Hangover",
        "overview": "The Hangover is a comedy movie",
        "year": 2019,
        "rating": 4.5,
        "category": "comedy",
    },
]


# generate a get operation with path "/html" that return a html response. this html response must have a h1 tag with the message "Hello World"
@movie_router.get("/html", tags=["home"])
def html():
    html_content = """
    <html>
        <head>
            <title>My app</title>
        </head>
        <body>
            <h1>Hello World</h1>
        </body>
    </html>
    """
    return HTTPResponse(content=html_content, status_code=200)


# generate a get operation with path "movies" thar return a dictionary. the method is calle get_movies and tag it with "movies"
@movie_router.get(
    "/movies2", tags=["movies"], response_model=List[Movie], status_code=200
)
def get_movies2() -> List[Movie]:
    # return the variable movies
    # return movies
    return JSONResponse(content=movies, status_code=200)


# generate a get operation with path "movies2" that return the variable movies as a json response. also have to dependes of the class JWTBearer to validate the token
@movie_router.get(
    "/movies",
    tags=["movies"],
    response_model=List[Movie],
    status_code=200,
    dependencies=[Depends(JWTBearer())],
)
def get_movies() -> List[Movie]:
    # return the variable movies
    # return movies
    # create session and store in variable db, next get all movies from db
    db = Session()
    result = MovieService(db).get_all()

    return JSONResponse(content=jsonable_encoder(result), status_code=200)


# generate another get operation with path "movies/{movie_id}" that return a dictionary. the method is called get_movie and tag it with "movies"
@movie_router.get("/movies/{movie_id}", tags=["movies"], response_model=Movie)
def get_movie(movie_id: int = Path(ge=1, le=2000)) -> Movie:
    # create db session and store in variable db
    db = Session()
    # get the movie with the id passed as parameter
    # result = db.query(MovieModel).filter(MovieModel.id == movie_id).first()
    result = MovieService(db).get_by_id(movie_id)
    # validate if the movie exist
    if result is None:
        raise HTTPException(status_code=404, detail="Movie not found")
    # return the movie
    return JSONResponse(content=jsonable_encoder(result), status_code=200)


# generate another get operation with path "movies" but this time  must receive a query parameter called "category". And return a list of movies that match with the category passed as parameter
@movie_router.get("/movies/", tags=["movies"], response_model=List[Movie])
def get_movies_by_category_and_year(
    category: str = Query(min_length=5, max_length=15, Optional=True, default=None),
) -> List[Movie]:
    # generate a session and store in variable db
    db = Session()
    # get all the movies with the category passed as parameter
    # result = db.query(MovieModel).filter(MovieModel.category == category).all()
    result = MovieService(db).get_by_category(category)
    # return the movies
    return JSONResponse(content=jsonable_encoder(result), status_code=200)


@movie_router.get("/movies2/", tags=["movies"], response_model=List[Movie])
def get_movies_by_category_and_year2(category: str, year: int):
    # return the movie with the id passed as parameter
    #     This code returns a list of items from the movies list that have a matching category and year.
    #  Step-by-step explanation:
    # 1. The code begins with the keyword "return" which indicates that the code will return a value.
    # 2. The code then creates a list using a list comprehension.
    # 3. The list comprehension iterates through each item in the movies list.
    # 4. For each item, the code checks if the item's "category" and "year" match the specified category and year.
    # 5. If both the category and year match, the item is added to the list.
    # 6. Finally, the list is returned.
    return [
        item for item in movies if item["category"] == category and item["year"] == year
    ]


# generate a post operation with path "movies" and id, title, overview, year, rating, category as parameters. the method is called create_movie and tag it with "movies"
@movie_router.post("/movies", tags=["movies"], response_model=dict)
def create_movie(
    movie: Movie,
) -> dict:
    # add the new movie to the list of movies
    # movies.append(movie)
    # create session in a variable called db and add the movie to the session
    db = Session()
    MovieService(db).create(movie)
    # return the movie
    return JSONResponse(
        content={"message": "se registro correctamente"}, status_code=201
    )


# generate a put operation with path "movies/{movie_id}" and id, title, overview, year, rating, category as parameters. the method is called update_movie and tag it with "movies"
@movie_router.put("/movies/{movie_id}", tags=["movies"], response_model=Movie)
def update_movie(
    movie_id: int,
    movie: Movie,
) -> Movie:
    # create a session and store in variable db
    db = Session()
    result = MovieService(db).update(movie_id, movie)
    # return the movie
    return JSONResponse(content=jsonable_encoder(result), status_code=200)


# generate a delete operation with path "movies/{movie_id}" that return nothing. the method is called delete_movie and tag it with "movies"
@movie_router.delete("/movies/{movie_id}", tags=["movies"], response_model=dict)
def delete_movie(movie_id: int) -> dict:
    # create a session and store in variable db
    db = Session()
    MovieService(db).delete(movie_id)
    # return a message
    return JSONResponse(
        content={"message": "se elimino correctamente"}, status_code=200
    )
