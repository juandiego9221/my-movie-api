# import the apirouter library
from fastapi import APIRouter

# import the base model library

# import the generate_token function from jwtmanager
from utils.jwtManager import generate_token

# import the jsonresponse library
from fastapi.responses import JSONResponse

# import user model from schema package
from schemas.user import User


# creat an user router
user_router = APIRouter()


# create a post operation with path "login" and "path" as tag that receive an User as input. it have to return the user
@user_router.post(
    "/login",
    tags=["auth"],
)
def login(user: User):
    if user.email == "jdmm21@example.com" and user.password == "12345678":
        token = generate_token(user.dict())
        #
    # return the user
    return JSONResponse(content=token, status_code=200)
