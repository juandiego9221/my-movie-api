from config.database import Base

# import from sql alchemy classes column, integer, string, text, DateTime, func
from sqlalchemy import Column, Integer, String, Text, DateTime, func


class Movie(Base):
    # table name "movies"
    __tablename__ = "movies"
    # the attributes of the table are id that is a primary key of type integer
    id = Column(Integer, primary_key=True)
    # the other attributes are title of type string , overview of type string , year of type integer , rating of type float and category of type string
    title = Column(String)
    overview = Column(String)
    year = Column(Integer)
    rating = Column(Integer)
    category = Column(String)
