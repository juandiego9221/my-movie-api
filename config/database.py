import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


sqllite_file_name = "database.sqlite"
"""
This code retrieves the base directory path of the file where it is being executed.
Explanation:
1. The code uses the  `os`  module to access certain system-dependent parameters and functions.
2.  `os.path.realpath(__file__)`  returns the absolute path of the file where the code is being executed.
3.  `os.path.dirname()`  returns the directory path of the given file path.
4. By applying  `os.path.dirname()`  twice, we get the base directory path of the file where the code is being executed.
5. The base directory path is stored in the  `base_dir`  variable.
"""
base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

"""
The code creates a URL for a SQLite database using the base directory and the name of the SQLite file. 
Step wise explanation:

1. The code starts by defining a variable called "database_url".
2. The f-string syntax is used to create a dynamic URL for the SQLite database.
3. The "os.path.join()" function is used to join the base directory path and the SQLite file name into a single path.
4. The resulting path is wrapped in "sqlite:/// " to indicate that it is a SQLite database URL.
5. The completed URL is then assigned to the "database_url" variable.
"""
database_url = f"sqlite:///{os.path.join(base_dir, sqllite_file_name)}"


"""
The code creates a database engine object and sets its configuration to output all SQL statements to the console.
 Explanation:
- The "create_engine" function is part of the SQLAlchemy library, which provides a high-level interface for working with databases in Python.
- The "database_url" parameter specifies the location and type of the database that the engine will connect to (e.g. "postgresql://user:password@host:port/database").
- The "echo" parameter is a Boolean flag that determines whether SQL statements executed by the engine will be printed to the console (True) or not (False).
- By setting "echo=True", the code enables SQL statement logging, which can be useful for debugging and performance tuning.
"""
engine = create_engine(database_url, echo=True)

"""
The given code creates a session object using SQLAlchemy's sessionmaker function.
 Step-by-step explanation:
 1. The code starts by calling the sessionmaker function provided by SQLAlchemy. This function is used to create a session class that can be used to interact with the database.
 2. The bind method is called on the sessionmaker object and it’s passed an engine object, which is typically created using SQLAlchemy’s create_engine function. This engine represents a connection to a database and provides a set of methods to interact with it.
 3. Once the session is created, it can be used to perform various database operations such as querying the database, adding new data, updating existing records, etc.
 Overall, the code initializes a session object that can be used to interact with the database in a more high-level and intuitive way.
"""
"""
En cuanto a la similitud específica con el entityManager de Hibernate en Java, en SQLAlchemy, su equivalente es el objeto Session. Tanto el entityManager en Hibernate como el Session en SQLAlchemy son responsables de administrar el contexto de persistencia, proporcionando métodos para realizar operaciones en la base de datos, como persistir entidades, cargar entidades desde la base de datos, realizar consultas y controlar transacciones.
"""
Session = sessionmaker(bind=engine)

"""
This code declares a base class for all ORM (Object-Relational Mapping) classes using the SQLAlchemy Python library.
 Explanation:
- SQLAlchemy is a popular Python library used for working with databases.
- ORM is a technique that allows developers to interact with databases using objects instead of writing raw SQL queries.
- The Base class is defined in the declarative module of SQLAlchemy, which is used for declarative ORM. 
- A class that inherits from Base can be mapped to a database table using the class attributes that define the table structure. 
- This makes it easier to create, query, update, and delete records in the database using Python code. 
- By declaring a Base class, the ORM classes can inherit from it and have a common set of functionality and attributes.
"""
Base = declarative_base()
