# import the Movie model
from model.movie import Movie as MovieModel

# import the Movie schema
from schemas.movie import Movie

from fastapi import HTTPException


class MovieService:
    # create constructor
    def __init__(self, db):
        self.db = db

    # create a method to get all movies
    def get_all(self):
        # get all movies from db
        result = self.db.query(MovieModel).all()
        # return the movies
        return result

    # create a method to get a movie by id
    def get_by_id(self, movie_id):
        # get the movie with the id passed as parameter
        result = self.db.query(MovieModel).filter(MovieModel.id == movie_id).first()
        # return the movie
        return result

    # create a method to filter movies by category
    def get_by_category(self, category):
        # get all movies from db
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        # return the movies
        return result

    # create a method to create a movie
    def create(self, movie: Movie):
        # create a MovieModel object with the movie parameter as kwargs
        """
        The selected code is creating a new instance of a MovieModel object using the MovieModel class and the dict() function. The ** operator is used to unpack the dictionary into keyword arguments for the MovieModel constructor. This is a common pattern in Python for creating objects from dictionaries.
        """
        movie_model = MovieModel(**movie.dict())
        # add the movie_model to the session
        self.db.add(movie_model)
        # commit the session
        self.db.commit()
        # return the movie
        return movie_model

    # create a method to update a movie
    def update(self, movie_id, movie: Movie):
        # find the movie with the id passed as parameter
        result = self.db.query(MovieModel).filter(MovieModel.id == movie_id).first()
        # validate if the movie exist
        if result is None:
            raise HTTPException(status_code=404, detail="Movie not found")
        # update the movie with the id passed as parameter
        result.title = movie.title
        result.overview = movie.overview
        result.year = movie.year
        result.rating = movie.rating
        result.category = movie.category
        # commit the session
        self.db.commit()
        # return the movie
        return result

    # create a method to delete a movie
    def delete(self, movie_id):
        # find the movie with the id passed as parameter
        result = self.db.query(MovieModel).filter(MovieModel.id == movie_id).first()
        # validate if the movie exist
        if result is None:
            raise HTTPException(status_code=404, detail="Movie not found")
        # delete the movie with the id passed as parameter
        self.db.delete(result)
        # commit the session
        self.db.commit()
        # return a message
        return
