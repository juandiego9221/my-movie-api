# import httpbearer
from fastapi.security import HTTPBearer

# import Request from fastapi
from fastapi import Request
from utils.jwtManager import validate_token

# import HTTPException from fastapi
from fastapi import HTTPException


# create a class called JWTBearer that extends from HTTPBearer. this class have to validate a token by using the functions generate_token and validate_token
class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data["email"] != "jdmm21@example.com":
            raise HTTPException(status_code=401, detail="Credenciales invalidas")
